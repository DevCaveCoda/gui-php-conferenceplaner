Conference Planner Backend
----------- 
### Installation
To run the application following requirements must be fulfilled:
- PHP 7.1 or higher
- Composer

### Configuration
Before the application can be used following Steps are needed:
- composer install
- Set correct API_URL in the .env file

### Local
The easiest way to run and test the application is in the dev-environment
than the symfony webserver-bundle is accessible and you can run the application with

- php bin/console server:run

### Usage
The GUI is accessible:

- http://127.0.0.1:8000/

Just start to add Talks, every time a talk is added the service reorganizes them, 
than sort them to sessions and talks.

Ps:
If the default data was imported in the service setup you have to create a new talk
before you see them. The default setup is inserted directly in the database and does not
trigger the reorganize function. 