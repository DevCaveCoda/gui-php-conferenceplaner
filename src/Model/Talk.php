<?php

namespace App\Model;

use JsonSerializable;

class Talk implements JsonSerializable
{
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var integer
     */
    private $length;

    /**
     * @var string
     */
    private $lang;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }

    /**
     * @param int $length
     */
    public function setLength(int $length): void
    {
        $this->length = $length;
    }

    /**
     * @return string
     */
    public function getLang(): string
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang(string $lang): void
    {
        $this->lang = $lang;
    }

    public function jsonSerialize()
    {
        return [
            'title'     => $this->getTitle(),
            'length' => $this->getLength(),
            'lang' => $this->getLang(),
        ];
    }
}
