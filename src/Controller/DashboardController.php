<?php


namespace App\Controller;


use App\Model\Talk;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class DashboardController extends AbstractController
{
    const TRACKS_URL = '/api/v1/tracks';
    const TALKS_URL = '/api/v1/talks';

    /**
     * @Route("/")
     * @param Request $request
     * @return Response
     */
    public function dashboard(Request $request) {
        $httpClient = HttpClient::create();
        $talk = new Talk();
        $talk->setTitle('Domain-driven Design Architektur Kata');
        $talk->setLength(60);
        $talk->setLang('de');

        $form = $this->createFormBuilder($talk)
            ->add('title', TextType::class)
            ->add('length', IntegerType::class)
            ->add('lang', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Talk'])
            ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            try {
                $httpClient->request('POST', $_ENV['API_URL'].self::TALKS_URL, ['json' => $form->getData()]);
            } catch (TransportExceptionInterface $e) {
                dump($e);
            }
        }

        try {
            $tracks = $httpClient->request('GET', $_ENV['API_URL'].self::TRACKS_URL)->toArray()['hydra:member'];
        } catch (ClientExceptionInterface $e) {
            $error = $e->getMessage();
        } catch (\Throwable $e) {
            dump($e);
            $error = $e->getMessage();
        }

        return $this->render('base.html.twig', [
            'tracks' => $tracks ?? [],
            'error' => $error ?? null,
            'form' => $form->createView(),
        ]);
    }
}